const express = require('express');
const router = express.Router();
const courseController = require('../controllers/course');
const auth = require('../auth');


router.post('', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true) {
		courseController.addCourse(req.body).then(resultFromController=> res.send(resultFromController))
	}else{
		return res.send("you're not an admin!, failed to create course")
	}
})


// Retrieve all the courses
router.get('/all' , (req,res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})


// retrieve all active courses
router.get('/' , (req, res) =>{
	courseController.getAllActice().then(resultFromController => res.send(resultFromController))
})

// retrieve specific course
router.get('/:courseId' , (req, res) => {
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// Update a course
router.put('/:courseId', auth.verify, (req, res)=> {
	courseController.updateCourse(req.params, req.body).then(resultFromController=> res.send(resultFromController))
})


// ACTIVITY
// archive course
router.put('/:courseId/archive', auth.verify, (req, res)=> {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true) {
		courseController.archiveCourse(req.params, req.body).then(resultFromController=> res.send(resultFromController))
	}else{
		return res.send("you're not an admin!, failed to archive course")
	}
})
module.exports = router;