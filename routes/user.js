const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');
const auth = require('../auth');

// checking email route
router.post('/checkEmail' , (req, res) =>{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))

})

// register route
router.post('/register' , (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})


// login route
router.post('/login' , (req,res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})



// ACTIVITY

// S34 CODE
// retrive the details of the user using id 
router.get('/details' , auth.verify,(req,res) =>{
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)

	userController.getProfile(req.body.id).then(resultFromController => res.send(resultFromController))
})


//For enrolling a user
router.post("/enroll", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	let data = {
		userId: userData.id,
		courseId: req.body.courseId
	}
	if(!userData.isAdmin){
		userController.enroll(data).then(resultFromController => 
		res.send(resultFromController));
	}else{
		res.send("Failed to enroll!")
	}
	

})


module.exports = router;