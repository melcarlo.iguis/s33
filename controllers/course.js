const Course = require('../models/Course');



// add course controller
module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course,error)=> {
		if(error){
			return false
		}else{
			console.log("course has been save")
			return true

		}
	})
}


// retrieve all courses controller
module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result;
	})
}


// retrieve all active course
module.exports.getAllActice = () => {
	return Course.find({isActive: true}).then(result =>{
		return result
	})
}

// Retrive specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
		// let getCourse = {
		// 	name: result.name,
		// 	price: result.price
		// }
		// return getCourse
	})
}

// Update a Course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	/*Syntax
		findByIdAndUpdate(document, updatesToBeApply)*/
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, err)=> {
		if(err){
			return false
		}else{
			return true
		}
	})
}

// ACTIVITY
// Archive course

module.exports.archiveCourse = (reqParams, reqBody) => {
	let updatedCourse = {

		isActive : reqBody.isActive
	}
	/*Syntax
		findByIdAndUpdate(document, updatesToBeApply)*/
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, err)=> {
		if(err){
			return false
		}else{
			return true
		}
	})
}


module.exports.getCourse