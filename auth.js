const jwt = require('jsonwebtoken');
const secret = 'CourseBookingAPI';


module.exports.createAccessToken = (user) =>{

	// The data will be received from the registration form
	// When the user logs in , a token will be created with user's information
	const data = {
		id: user.id,
		email : user.email,
		isAdmin: user.isAdmin
	}


	// Generate a JSON web token using jwt's 'sign' method
	// generate the token using the form data, and the secret code with no additional options provided.
	return jwt.sign(data, secret, {})
}


// S34 CODE

module.exports.verify = (req, res, next	) => {
	// The data is retrieved from the request header
	let token = req.headers.authorization;

	// Token received and not undefined
	if(typeof token !== "undefined"){
		console.log(token)


		// Bearer : awjgehjwae
		// to remove the 'bearer' and retrieve only the jwt code we use slice method
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err,data) =>{
			if(err){
				return res.send({auth: "failed"})
			}else{


				next()
			}
		})
	}else{
		return res.send({auth: "failed"})
	}
}


module.exports.decode = (token) => {

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret,(err, data) => {

			if(err){
				return null
			}else{
				return jwt.decode(token, {complete:true}).payload
			}
		})
	}else{
		return null
	}
}

// module.exports.checkIsAdmin = (req, res, next	) => {
// 	// The data is retrieved from the request header
// 	let token = req.headers.authorization;

// 	// Token received and not undefined
// 	if(typeof token !== "undefined"){
// 		console.log(token)

// 		// Bearer : awjgehjwae
// 		// to remove the 'bearer' and retrieve only the jwt code we use slice method
// 		token = token.slice(7, token.length)

// 		return jwt.verify(token, secret, (err,data) =>{
// 			console.log(data.isAdmin)
// 			let admin = data.isAdmin == true;
// 			if(admin){
// 				next()
// 				return res.send("successfully create a course")
// 			}else{
// 				return res.send("you're not an admin")
// 				return res.send({auth: "failed"})
				
// 			}
// 		})
// 	}else{
// 		return res.send({auth: "failed"})
// 	}
// }
